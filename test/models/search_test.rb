require 'test_helper'

class SearchTest < ActiveSupport::TestCase
  test "status should be pending by default" do
    search = Search.new
    assert_equal 'pending', search.status
  end

  test "should have a ip" do
    summary = Search.create(ip: '')
    assert_equal ["can't be blank"], summary.errors[:ip]
  end

  test "should have a text" do
    summary = Search.create(text: '')
    assert_equal ["can't be blank"], summary.errors[:text]
  end

  test ".expired should get the expired searches" do
    Search.delete_all

    Search.create(ip: '127.0.0.1', text: 'How to cancel my account', updated_at: 10.seconds.ago)
    Search.create(ip: '127.0.0.2', text: 'Where is my products',     updated_at: 9.seconds.ago)
    Search.create(ip: '127.0.0.3', text: 'How to',                   updated_at: 3.seconds.ago)
    Search.create(ip: '127.0.0.4', text: 'How',                      updated_at: 1.seconds.ago)
    Search.create(ip: '127.0.0.1', text: 'Where',                    updated_at: 1.seconds.ago)

    assert_equal 5, Search.count
    assert_equal 2, Search.expired.count
    assert_equal 'How to cancel my account', Search.expired.first.text
    assert_equal 'Where is my products',     Search.expired.second.text
  end
end
