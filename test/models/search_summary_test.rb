require 'test_helper'

class SearchSummaryTest < ActiveSupport::TestCase
  test "should have a text" do
    summary = SearchSummary.create(text: '')
    assert_equal ["can't be blank"], summary.errors[:text]
  end

  test "should have unique text" do
    q = 'Can you help me?'
    SearchSummary.create!(text: q)
    summary = SearchSummary.create(text: q)
    assert_equal ['has already been taken'], summary.errors[:text]
  end
end
