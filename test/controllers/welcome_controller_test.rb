require 'test_helper'

class WelcomeControllerTest < ActionDispatch::IntegrationTest
  include ActiveJob::TestHelper

  test "should get index" do
    get welcome_index_url
    assert_response :success
  end

  test "should get reset clear the searches and summaries" do
    assert Search.any?
    assert SearchSummary.any?

    get reset_url

    assert !Search.any?
    assert !SearchSummary.any?
  end

  test "should get reset redirect to home" do
    get reset_url
    assert_redirected_to root_url
  end

  test "should get job enqueue the SearchSummaryJob" do
    get job_url
    assert_enqueued_jobs 1
  end
end
