require 'test_helper'

class SearchControllerTest < ActionDispatch::IntegrationTest
  setup do
    @q = 'How can I update my payment way'
  end

  test "should get index" do
    get search_index_url
    assert_response :success
  end

  test "should create a new search record" do
    assert_difference 'Search.count', 1 do
      get search_index_url(q: @q)
    end
  end

  test "should not create a new search record if query is blank" do
    assert_difference 'Search.count', 0 do
      get search_index_url(q: '')
    end
  end

  test "should set the 'text' attribute" do
    get search_index_url(q: @q)
    assert_equal @q, Search.last.text
  end

  test "should set the 'ip' attribute" do
    get search_index_url(q: @q)
    assert_equal '127.0.0.1', Search.last.ip
  end

  test "should keep the 'status' attribute with 'pending'" do
    get search_index_url(q: @q)
    assert_equal 'pending', Search.last.status
  end

  test "should update my recentest search instead of create a new one" do
    my    = Search.create(text: 'How can I', ip: '127.0.0.1')
    other = Search.create(text: 'Where is ', ip: '127.0.0.2')

    assert_difference 'Search.count', 0 do
      get search_index_url(q: 'How can I find my products')
    end

    my.reload
    assert_equal 'How can I find my products', my.text
  end

  test "should not update my recentest if have passed more than 5 seconds" do
    my    = Search.create(text: 'How can I', ip: '127.0.0.1', updated_at: Time.now - 6.seconds)
    other = Search.create(text: 'Where is ', ip: '127.0.0.2')

    assert_difference 'Search.count', 1 do
      get search_index_url(q: 'How can I find my products')
    end

    my.reload
    assert_equal 'How can I', my.text
  end
end
