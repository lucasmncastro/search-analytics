require 'test_helper'

class SearchSummaryJobTest < ActiveJob::TestCase
  test "should account all the expired searches" do
    SearchSummary.delete_all
    s1 = Search.create(ip: '127.0.0.1', text: 'How to cancel my account', updated_at: 7.seconds.ago)
    s2 = Search.create(ip: '127.0.0.2', text: 'Where are my products',    updated_at: 6.seconds.ago)

    SearchSummaryJob.perform_now

    assert_equal 2, SearchSummary.count
    assert_equal 1, SearchSummary.first.hits
    assert_equal 1, SearchSummary.second.hits
  end

  test "should update the existing summaries" do
    SearchSummary.delete_all
    SearchSummary.create(text: 'WHERE ARE MY PRODUCTS', hits: 4)

    Search.create(ip: '127.0.0.2', text: 'Where are my products', updated_at: 6.seconds.ago)
    SearchSummaryJob.perform_now

    assert_equal 1, SearchSummary.count
    assert_equal 5, SearchSummary.first.hits
  end

  test "should ignore the text case" do
    SearchSummary.delete_all
    Search.create(ip: '127.0.0.2', text: 'Where are my products', updated_at: 7.seconds.ago)
    Search.create(ip: '127.0.0.3', text: 'Where Are My Products', updated_at: 6.seconds.ago)

    SearchSummaryJob.perform_now

    assert_equal 1, SearchSummary.count
    assert_equal 2, SearchSummary.first.hits
  end

  test "should turn the expired searches processed" do
    s1 = Search.create(ip: '127.0.0.1', text: 'How to cancel my account', updated_at: 7.seconds.ago)
    s2 = Search.create(ip: '127.0.0.2', text: 'Where are my products',    updated_at: 6.seconds.ago)

    SearchSummaryJob.perform_now

    s1.reload
    s2.reload

    assert_equal 'processed', s1.status
    assert_equal 'processed', s2.status
  end
end
