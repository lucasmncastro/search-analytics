Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  get 'welcome/index'
  get 'reset'  => 'welcome#reset', as: :reset
  get 'job'    => 'welcome#job',   as: :job

  get 'search' => 'search#index', as: :search_index

  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
