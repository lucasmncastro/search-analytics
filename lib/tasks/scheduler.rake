desc "Delete searches and summaries"
task :search_reset => :environment do
  Search.reset
  puts "done."
end

desc "Proccess searches to count hits - job executed periodically."
task :search_proccess => :environment do
  SearchSummaryJob.perform_later
  puts "done."
end
