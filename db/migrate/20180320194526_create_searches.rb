class CreateSearches < ActiveRecord::Migration[5.1]
  def change
    create_table :searches do |t|
      t.string :text
      t.integer :status, default: 0
      t.string :ip

      t.timestamps
    end
  end
end
