class CreateSearchSummaries < ActiveRecord::Migration[5.1]
  def change
    create_table :search_summaries do |t|
      t.string :text
      t.integer :hits, default: 0

      t.timestamps
    end
  end
end
