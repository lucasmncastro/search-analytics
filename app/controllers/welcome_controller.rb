class WelcomeController < ApplicationController
  def index
    @summaries = SearchSummary.order(hits: :desc).limit(10)
  end

  def reset
    Search.reset
    redirect_to root_url
  end

  def job
    SearchSummaryJob.perform_later
    redirect_to root_url
  end
end
