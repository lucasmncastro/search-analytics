class Search < ApplicationRecord
  # Maximum time in which a Search can be updated or it will be considered
  # expired.
  #
  # After this time we are assuming the sentence is complete.
  TIME_TO_EXPIRE = 5.seconds

  # This helps us to control which Searches need be accounted for by the
  # background job.
  enum status: [:pending, :processing, :processed]

  # Empty search doesn't matter for us.
  # They also need the IP.
  validates :text, :ip, presence: true

  # Get all searches that will not be updated no more.
  scope :expired, -> { where('updated_at < ?', Time.current - TIME_TO_EXPIRE) }


  # Look for the last search for this IP within the expiration period
  # (TIME_TO_EXPIRE) and then update the text. 
  #
  # If nothing is found, it creates a new record.
  def self.save(ip, text)
    limit_time  = Time.current - TIME_TO_EXPIRE
    last_search = Search.where('ip = ? and updated_at > ?', ip, limit_time)
                        .order('updated_at ASC').last

    if last_search
      last_search.update(text: text)
      last_search
    else
      Search.create(text: text, ip: ip)
    end
  end

  # Clear all the Searches and SearchSummaries in database.
  def self.reset
    Search.delete_all
    SearchSummary.delete_all
  end
end
