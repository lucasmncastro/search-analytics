class SearchSummary < ApplicationRecord
  validates :text, presence: true, uniqueness: true
end
