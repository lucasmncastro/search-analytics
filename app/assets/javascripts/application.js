// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require jquery
//= require_tree .

document.addEventListener("turbolinks:load", function() {
  $('.search-box input').typeWatch({
    // Call the search action.
    callback: function(value) {
      $('.loader').removeClass('d-none')

      $.get('/search', {q: value}, function() {
        // This keeps the loading message for at least a half second.
        setTimeout(function() {
          $('.loader').addClass('d-none')
        }, 500);
      })
    },
    wait: 750,         // Time waiting for a type before call the function.
    allowSubmit: true, // Allow to submit with Enter.
    captureLength: 3   // Minimum size to call the function.
  })
})
