class SearchSummaryJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Search.expired.where(status: :pending).find_each do |search|
      search.update status: :processing

      summary = SearchSummary.find_or_create_by(text: search.text.strip.upcase)
      summary.increment! :hits

      search.update status: :processed
    end
  end
end
